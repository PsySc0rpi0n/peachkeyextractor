from bitcoinlib.keys import HDKey
from bitcoinlib.mnemonic import Mnemonic
from bitcoinlib.encoding import base58encode
from bitcoinlib.encoding import double_sha256


def extract_keys(addr_type=0, addr_idx=20):
    """
        This function takes two parameters.
        First allow us to choose between Public Receiving Addresses setting
        it to 0 or Change Addresses or also known as Internal Addresses by
        setting it to 1. Defaults to 0.
        The second parameter let us choose how many addresses to derive from
        the Seed Phrase. Defaults to 20 (from 0 to 19).
        The derivation path is set to BPI84 and for Bitcoin coin.
    """

    for addr_index in range(addr_idx):
        # Derive a private key using the specific derivation path
        key = hdkey.subkey_for_path("m/84'/0'/0'/" + str(addr_type) + "/" +
                                    str(addr_index))

        # Get the corresponding native SegWit address
        bitcoin_address = key.address(encoding='bech32')

        # Prepend and append the network byte (0x80) and the compression
        # flag (0x01)
        prvKey_data = (b'\x80' + key.private_byte + b'\x01')

        # Calculate the checksum
        checksum = double_sha256(prvKey_data)

        # Prepend the checksum to the double sha256'ed key
        private_key_base58 = base58encode(prvKey_data + checksum[0:4])
        if not addr_type:
            print(f"addr: {addr_index + 1}: {bitcoin_address}")
            print(f"Private key {addr_index + 1}: {private_key_base58}")
        else:
            print(f"change addr: {addr_index + 1}: {bitcoin_address}")
            print(f"Private key: {addr_index + 1}: {private_key_base58}")


# Example seed phrase (mnemonic)
mnemonic_phrase = "thunder trade sleep rack pair ranch sense hand inquiry gauge you know"

# Create a Mnemonic object
mnemonic = Mnemonic()
# Convert the mnemonic phrase to a seed (binary)
seed = mnemonic.to_seed(mnemonic_phrase)

# Create an HD wallet from the seed
hdkey = HDKey.from_seed(seed, network='bitcoin')


extract_keys(1, 10)
